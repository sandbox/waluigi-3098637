<?php

namespace Drupal\simple_monitoring\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\simple_monitoring\MetricPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for our metrics.
 */
class SimpleMonitoringController extends ControllerBase {

  /**
   * The metrics manager.
   *
   * Used to get all the metric plugins.
   *
   * @var \Drupal\simple_monitoring\MetricPluginManager
   */
  protected $metricManager;

  /**
   * SimpleMonitoringController constructor.
   *
   * @param \Drupal\simple_monitoring\MetricPluginManager $metricManager
   *   The metric plugin manager.
   */
  public function __construct(MetricPluginManager $metricManager) {
    $this->metricManager = $metricManager;
  }

  /**
   * {@inheritdoc}
   *
   * Override the parent method so that we can inject our sandwich plugin
   * manager service into the controller.
   *
   * For more about how dependency injection works read
   * https://www.drupal.org/node/2133171
   *
   * @see container
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('plugin.manager.metric'));
  }

}
