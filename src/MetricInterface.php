<?php

namespace Drupal\simple_monitoring;

/**
 * Interface MetricInterface.
 */
interface MetricInterface {

  /**
   * Provide a description of the metric.
   *
   * @return string
   *   A String description of the metric.
   */
  public function description();

  /**
   * Returns the data of a desired metric.
   *
   * @return array
   *   The array containing the data.
   */
  public function getMetricResult();

  /**
   * Returns the human readable name of a metric.
   *
   * @return string
   *   Name of the metric.
   */
  public function humanReadable();

}
