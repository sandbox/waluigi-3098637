<?php

namespace Drupal\simple_monitoring\Commands;

use Drupal\simple_monitoring\MetricPluginManager;
use Drush\Commands\DrushCommands;

/**
 * {@inheritDoc}
 */
class SimpleMonitoringCommands extends DrushCommands {

  /**
   * The plugin manager.
   *
   * @var \Drupal\simple_monitoring\MetricPluginManager
   */
  protected $pluginManager;

  /**
   * {@inheritDoc}
   */
  public function __construct(MetricPluginManager $pluginManager) {
    $this->pluginManager = $pluginManager;
    parent::__construct();
  }

  /**
   * Echos back the defined metrics in the defined format.
   *
   * @param array $options
   *   Options array.
   *
   * @command simple-simple_monitoring:get-metrics
   * @aliases sm-gm
   * @options arr An option that takes multiple values.
   * @usage simple-simple_monitoring:get-metrics
   *   Display the collected metrics.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If the instance cannot be created, such as if the ID is invalid.
   *
   * @return array
   *   The metrics array.
   */
  public function getMetrics(array $options = ['format' => '']) {
    $definitions = $this->pluginManager->getActiveDefinitions();
    $metrics = [];
    foreach ($definitions as $definition) {
      $id = $definition['id'];

      /** @var $metric \Drupal\simple_monitoring\MetricResult */
      $metric = $this->pluginManager->createInstance($id)->getMetricResult();
      $metrics[$id] = $metric->render();
    }

    return $metrics;
  }

}
