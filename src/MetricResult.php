<?php

namespace Drupal\simple_monitoring;

/**
 * Used, to restrict the array structure of a metric to avoid unwanted data.
 *
 * @package Drupal\simple_monitoring
 */
class MetricResult {

  /**
   * The title of the metric.
   *
   * @var string
   */
  protected $title;

  /**
   * The status code of the metric.
   *
   * By default this is on error, respectively 0
   * Set it to 1 for success and 2 for warnings.
   *
   * @var int
   */
  protected $statuscode;

  /**
   * Unix timestamp of the last refresh.
   *
   * Gets generated when object is constructed.
   *
   * @var int
   */
  protected $lastRefresh;

  /**
   * Array containing the actual metrics data.
   *
   * @var array
   */
  protected $data;

  /**
   * MetricResult constructor.
   *
   * @param string $title
   *   The human readable title of the metric.
   */
  public function __construct($title) {
    // Set status code to 'error', respectively 0, by default.
    $this->statuscode = 2;
    $this->lastRefresh = \Drupal::time()->getCurrentTime();
    $this->data = [];
  }

  /**
   * Append a new sub metric to the result.
   *
   * @param string $title
   *   The title or rather the name of the sub metric.
   * @param int $statusCode
   *   The status code of the metric.
   *   Set 0 for success, 1 for warning and 2 for error.
   * @param string $value
   *   The string value for the given title.
   */
  public function addData($title, $statusCode, $value) {
    $this->data[] = [
      'title' => $title,
      'statuscode' => $statusCode,
      'value' => $value,
    ];
  }

  /**
   * Returns the array with the gathered information.
   *
   * @return array
   *   The array containing the data.
   */
  public function render() {
    return [
      'title' => $this->getTitle(),
      'statuscode' => $this->getStatuscode(),
      'last_refresh' => $this->getLastRefresh(),
      'data' => $this->data,
    ];
  }

  /**
   * Getter for title.
   *
   * @return string
   *   The title.
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Setter for title.
   *
   * @param string $title
   *   The title.
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * Getter for status code.
   *
   * @return int
   *   The status code.
   */
  public function getStatuscode() {
    return $this->statuscode;
  }

  /**
   * Setter for status code.
   *
   * @param int $statuscode
   *   The status code.
   */
  public function setStatuscode($statuscode) {
    $this->statuscode = $statuscode;
  }

  /**
   * Get last refresh timestamp.
   *
   * @return int
   *   Unix timestamp of last refresh.
   */
  protected function getLastRefresh() {
    return $this->lastRefresh;
  }

}
