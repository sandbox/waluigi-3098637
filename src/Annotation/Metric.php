<?php

namespace Drupal\simple_monitoring\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Metric annotation object.
 *
 * @see \Drupal\simple_monitoring\MetricPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class Metric extends Plugin {
  /**
   * A brief, human readable description of the metric type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * Human readable name of the metric type.
   *
   * @var string
   */
  public $humanReadable;

}
