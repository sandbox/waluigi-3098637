<?php

namespace Drupal\simple_monitoring;

use Drupal\Core\Plugin\PluginBase;

/**
 * {@inheritDoc}
 */
class MetricBase extends PluginBase implements MetricInterface {

  /**
   * Returns the description of the plugin.
   *
   * @return string
   *   The description.
   */
  public function description() {
    return $this->pluginDefinition['description'];
  }

  /**
   * Returns default data every metric needs to implement.
   *
   * @return \Drupal\simple_monitoring\MetricResult
   *   The metric result.
   */
  public function getMetricResult() {
    $title = $this->humanReadable();
    $metric = new MetricResult($title);
    return $metric;
  }

  /**
   * Returns a the human readable name of the metric.
   *
   * @return string
   *   The human readable name of th metric.
   */
  public function humanReadable() {
    return $this->pluginDefinition['humanReadable'];
  }

}
