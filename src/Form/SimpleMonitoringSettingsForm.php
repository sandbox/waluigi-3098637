<?php

namespace Drupal\simple_monitoring\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SimpleMonitoringSettingsForm extends ConfigFormBase {
  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'simple_monitoring.settings';


  /**
   * The plugin manager.
   *
   * @var \Drupal\simple_monitoring\MetricPluginManager
   */
  protected $pluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.metric')
    );
  }

  /**
   * SimpleMonitoringSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $pluginManager
   *   The plugin manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, PluginManagerInterface $pluginManager) {
    $this->pluginManager = $pluginManager;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_monitoring_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $plugins = $this->pluginManager->getActiveDefinitions();
    $plugins = $this->pluginManager->getDefinitions();
    $this->pluginManager->createInstance('default_metric')->description();
    $options = [];
    foreach ($plugins as $key => $plugin) {
      $options[$key] = $plugin['humanReadable'] . ' - ' . $plugin['description'];
    }

    $form['plugins'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $config->get('plugins'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable(static::SETTINGS);
    $options = [];
    foreach ($form_state->getValues()['plugins'] as $key => $setting) {
      $options[$key] = $setting;
    }

    $config->set('plugins', array_values($options));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
