<?php

namespace Drupal\simple_monitoring;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\simple_monitoring\Annotation\Metric;

/**
 * {@inheritDoc}
 */
class MetricPluginManager extends DefaultPluginManager {

  /**
   * {@inheritDoc}
   */
  public function __construct(\Traversable $namespaces,  $cache_backend, ModuleHandlerInterface $module_handler) {
    $subdir = 'Plugin/Metric';
    $plugin_interface = MetricInterface::class;
    $plugin_definition_annotation_name = Metric::class;

    parent::__construct($subdir, $namespaces, $module_handler, $plugin_interface, $plugin_definition_annotation_name);
  }

  /**
   * Gets the definition of all plugins set on active in the config form.
   *
   * If none are selected, all are active.
   *
   * @return mixed[]
   *   An array of active plugin definitions (empty array if no definitions were
   *   found). Keys are plugin IDs.
   */
  public function getActiveDefinitions() {
    $definitions = $this->getDefinitions();
    $config = \Drupal::config('simple_monitoring.settings');
    $plugins = $config->get('plugins');
    $selected = [];

    foreach ($definitions as $definition) {
      $id = $definition['id'];
      if (in_array($id, $plugins)) {
        $selected[$id] = $definition;
      }
    }
    return $selected;
  }

}
