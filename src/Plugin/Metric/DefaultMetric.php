<?php

namespace Drupal\simple_monitoring\Plugin\Metric;

use Drupal\simple_monitoring\MetricBase;

/**
 * Provides information about the core.
 *
 * @Metric(
 *   id = "default_metric",
 *   description = @Translation("Does nothing else than tell you that you need to enable other plugins"),
 *   humanReadable = "Default Metric"
 * )
 */
class DefaultMetric extends MetricBase {

  /**
   * {@inheritDoc}
   */
  public function getMetricResult() {
    $metric = parent::getMetricResult();
    $metric->setTitle('Default Metric');
    $metric->addData('Default', 2, 'This is the default plugin. Disable it after installation!');
    return $metric;
  }

}
