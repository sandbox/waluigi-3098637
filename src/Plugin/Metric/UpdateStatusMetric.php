<?php

namespace Drupal\simple_monitoring\Plugin\Metric;

use Drupal\simple_monitoring\MetricBase;

/**
 * Provides the update status for each module.
 *
 * @Metric(
 *   id = "update_metric",
 *   description = @Translation("Checks if any updates are available"),
 *   humanReadable = "Update Metric"
 * )
 */
class UpdateStatusMetric extends MetricBase {

  /**
   * {@inheritDoc}
   */
  public function getMetricResult() {
    $metric = parent::getMetricResult();
    $metric->setTitle('Update Status');

    $available = update_get_available();
    $modules = update_calculate_project_data($available);

    foreach ($modules as $module) {
      $title = $module['title'];

      if ($module['existing_version'] !== $module['recommended']) {
        $statusCode = 2;
        $value = 'Update recommended';
      }
      elseif ($module['existing_version'] !== $module['latest_version']) {
        $statusCode = 1;
        $value = 'Update available';
      }
      else {
        $statusCode = 0;
        $value = 'Up to date!';
      }

      $metric->addData($title, $statusCode, $value);
    }
    $metric->setStatuscode(2);
    return $metric;
  }

}
