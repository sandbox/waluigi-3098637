<?php

namespace Drupal\simple_monitoring\Plugin\Metric;

use Drupal\simple_monitoring\MetricBase;

/**
 * Provides information about the core.
 *
 * @Metric(
 *   id = "core_metric",
 *   description = @Translation("Provides information about the core system"),
 *   humanReadable = "Core Metric"
 * )
 */
class CoreStatusMetric extends MetricBase {

  /**
   * {@inheritDoc}
   */
  public function getMetricResult() {
    $metric = parent::getMetricResult();
    $metric->setTitle('Core Status');
    $metric->addData('Core Version', 0, \Drupal::VERSION);
    $metric->setStatuscode(0);
    return $metric;
  }

}
