<?php

use Drupal\Tests\UnitTestCase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\simple_monitoring\Plugin\Metric\DefaultMetric;
use Symfony\Component\DependencyInjection\Container;
use Drupal\simple_monitoring\Plugin\Metric\CoreStatusMetric;

/**
 * Class CoreStatusMetric.
 *
 * @group simple_monitoring
 */
class MetricTest extends UnitTestCase {


  /**
   * The container mock.
   *
   * @var \Symfony\Component\DependencyInjection\Container
   */
  public $container;

  /**
   * {@inheritDoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->container = new Container();
    $time = $this->createMock("\Drupal\Component\Datetime\TimeInterface");
    $this->container->set('datetime.time', $time);

  }

  /**
   * Tests Default Metric.
   */
  public function testDefaultMetric() {
    $description = new TranslatableMarkup("Does nothing else than tell you that you need to enable other plugins");
    $definition = [
      'id' => 'default_metric',
      'description' => $description,
      'humanReadable' => 'Default Metric',
      'class' => "Drupal\simple_monitoring\Plugin\Metric\DefaultMetric",
      'provider' => 'simple_monitoring',
    ];
    $metric = new DefaultMetric([], 'default_metric', $definition);

    \Drupal::setContainer($this->container);

    // Set example array for metric result.
    $example = [
      'title' => NULL,
      'statuscode' => 0,
      'last_refresh' => NULL,
      'data' => [
        [
          'title' => "Default",
          'statuscode' => 2,
          'value' => "This is the default plugin. Disable it after installation!",
        ],
      ],
    ];

    $this->assertTrue($metric->humanReadable() === "Default Metric");
    $this->assertEquals($description, $metric->description());
    $this->assertArrayEquals($example, $metric->getMetricResult()->render());
  }

  /**
   * Tests Core Status Metric.
   */
  public function testCoreStatusMetric() {
    $description = new TranslatableMarkup("Test Description");
    $definition = [
      'id' => 'core_metric',
      'description' => $description,
      'humanReadable' => 'Core Metric',
      'class' => "Drupal\simple_monitoring\Plugin\Metric\CoreStatusMetric",
      'provider' => 'simple_monitoring',
    ];
    $metric = new CoreStatusMetric([], 'default_metric', $definition);

    \Drupal::setContainer($this->container);

    $this->assertTrue($metric->humanReadable() === "Core Metric");
    $this->assertEquals($description, $metric->description());
  }

}
